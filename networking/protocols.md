# TCP


---
# UDP


---
# FTP


---
# SMTP


---
# SNMP


---
# HTTP

>HyperText Transfer Protocol

- Giao thức Vận chuyển Siêu văn bản
- Là một giao thức lớp ứng dụng cho các hệ thống thông tin siêu phương tiện phân tán, cộng tác.
- HTTP là nền tảng của truyền thông dữ liệu cho World Wide Web, nơi siêu văn bản tài liệu bao gồm các siêu liên kết đến các tài nguyên khác mà người dùng có thể dễ dàng truy cập, ví dụ bằng một con chuột nhấp chuột hoặc bằng cách chạm vào màn hình trong một trình duyệt web.

Sự phát triển của HTTP được Tim Berners-Lee tại CERN khởi xướng vào năm 1989. Việc phát triển các RFC HTTP ban đầu là một nỗ lực phối hợp của Lực lượng Đặc nhiệm Kỹ thuật Internet (IETF) và World Wide Web Consortium (W3C), với các thay đổi sau đó chuyển sang do IETF phụ trách.

## HTTP Status Codes

### Categories

-   **1XX** status codes: Informational Requests
-   **2XX** status codes: Successful Requests
-   **3XX** status codes: Redirects
-   **4XX** status codes: Client Errors
-   **5XX** status codes: Server Errors

### Complete List

Code|Status|Description|Description (VN)
---|---|---|---
100|Continue|Everything so far is OK and that the client should continue with the request or ignore it if it is already finished.|Máy chủ trả về mã này để chỉ ra rằng nó đã nhận được một phần đầu tiên của một yêu cầu và được chờ đợi cho phần còn lại.
101|Switching Protocols|The client has asked the server to change protocols and the server has agreed to do so.|Client đã yêu cầu máy chủ thay đổi giao thức và máy chủ đã đồng ý làm như vậy.
102|Processing|The server has received and is processing the request, but that it does not have a final response yet.|Bên yêu cầu đã yêu cầu các máy chủ để chuyển đổi và máy chủ được thừa nhận rằng nó sẽ làm như vậy
103|Early Hints|Used to return some response headers before final HTTP message.|Được sử dụng để trả về một số tiêu đề phản hồi trước thông báo HTTP cuối cùng.
200|OK|Successful request.|Các máy chủ xử lý yêu cầu thành công.
201|Created|The server acknowledged the created resource.|Yêu cầu đã thành công và các máy chủ tạo ra một nguồn tài nguyên mới.
202|Accepted|The client's request has been received but the server is still processing it.|Máy chủ đã chấp nhận yêu cầu, nhưng vẫn chưa xử lý nó.
203|Non-Authoritative Information|The response that the server sent to the client is not the same as it was when the server sent it.|Máy chủ xử lý yêu cầu thành công, nhưng đang quay trở lại các thông tin mà có thể là từ một nguồn khác.
204|No Content|There is no content to send for this request.|Các máy chủ xử lý yêu cầu thành công, nhưng không trả lại bất kỳ nội dung nào.
205|Reset Content|Tells the user agent to reset the document which sent this request.|Các máy chủ proccessed yêu cầu thành công, nhưng không trả lại bất kỳ nội dung. Không giống như một phản ứng 204, phản ứng này đòi hỏi người yêu cầu thiết lập lại xem tài liệu.
206|Partial Content|This response code is used when the range-header is sent from the client to request only part of a resource.|Các máy chủ xử lý thành công một phần của một yêu cầu.
207|Multi-Status|Conveys information about multiple resources, for situations where multiple status codes might be appropriate.|Truyền tải thông tin về nhiều tài nguyên, cho các tình huống trong đó nhiều mã trạng thái có thể phù hợp.
208|Already Reported|The members of a DAV binding have already been enumerated in a preceding part of the multi-status response.|Các thành viên của ràng buộc DAV đã được liệt kê trong một phần trước của phản hồi đa trạng thái.
226|IM Used|IM is a specific extension of the HTTP protocol. The extension allows a HTTP server to send diffs (changes) of resources to clients.|IM là một phần mở rộng cụ thể của giao thức HTTP. Tiện ích mở rộng cho phép máy chủ HTTP gửi các khác biệt (thay đổi) tài nguyên đến clients.
300|Multiple Choices|The request has more than one possible response. The user agent should choose one.|Yêu cầu có nhiều hơn một phản hồi có thể có. Tác nhân người dùng nên chọn một.
301|Moved Permanently|The URL of the requested resource has been changed permanently. The new URL is given in the response.|Các trang web yêu cầu đã bị di chuyển vĩnh viễn tới URL mới.
302|Found|This response code means that the URI of requested resource has been changed temporarily.|Trang được yêu cầu đã di chuyển tạm thời tới một URL mới.
303|See Other|The server sent this response to direct the client to get the requested resource at another URI with a GET request.|Máy chủ đã gửi phản hồi này để hướng dẫn client lấy tài nguyên được yêu cầu tại một URI khác với yêu cầu GET.
304|Not Modified|It tells the client that the response has not been modified, so the client can continue to use the same cached version of the response.|Các trang yêu cầu đã không được sửa đổi kể từ khi yêu cầu cuối cùng. Khi máy chủ trả về phản hồi này, nó không trả lại các nội dung của trang.
305|Use Proxy|Defined in a previous version of the HTTP specification to indicate that a requested response must be accessed by a proxy. (discontinued)|Được xác định trong phiên bản trước của đặc tả HTTP để chỉ ra rằng phản hồi được yêu cầu phải được truy cập bởi proxy. (đã ngừng)
307|Temporary Redirect|The server sends this response to direct the client to get the requested resource at another URI with same method that was used in the prior request.|Máy chủ gửi phản hồi này để hướng dẫn client lấy tài nguyên được yêu cầu tại một URI khác với cùng một phương thức đã được sử dụng trong yêu cầu trước đó.
308|Permanent Redirect|This means that the resource is now permanently located at another URI, specified by the Location: HTTP Response header.|Điều này có nghĩa là tài nguyên hiện được đặt vĩnh viễn tại một URI khác, được chỉ định bởi tiêu đề Vị trí: Phản hồi HTTP.
400|Bad Request|The server could not understand the request.|Các máy chủ không hiểu được yêu cầu.
401|Unauthorized|The client didn't authenticate himself.|Đề nghị yêu cầu xác thực. Máy chủ có thể trả về phản hồi này yêu cầu xác thực đăng nhập tài khoản và mật khẩu (thông thường máy chủ trả về phản hồi này nếu client gửi request một trang đăng nhập)
402|Payment Required|This response code is reserved for future use. The initial aim for creating this code was using it for digital payment systems, however this status code is used very rarely and no standard convention exists.|Mã phản hồi này được dành riêng để sử dụng trong tương lai. Mục đích ban đầu để tạo mã này là sử dụng nó cho các hệ thống thanh toán kỹ thuật số, tuy nhiên mã trạng thái này rất hiếm khi được sử dụng và không có quy ước tiêu chuẩn nào tồn tại.
403|Forbidden|The client does not have access rights to the content.|Máy chủ từ chối yêu cầu.(thông thường nếu đăng nhập không thành công máy chủ sẽ trả về mã lỗi này)
404|Not Found|The server can not find the requested resource.|Máy chủ không thể tìm thấy trang yêu cầu. Ví dụ, máy chủ thường trả về mã này nếu có 1 yêu cầu tới một trang không tồn tại trên máy chủ.
405|Method Not Allowed|The request method is known by the server but is not supported by the target resource.|Phương thức được xác định trong yêu cầu là không được cho phép.
406|Not Acceptable|The reponse doens't conforms to the creteria given by the client|Server chỉ có thể tạo một phản hồi mà không được chấp nhận bởi Client.|Phương thức được xác định trong yêu cầu là không được cho phép.
407|Proxy Authentication Required|This is similar to 401 Unauthorized but authentication is needed to be done by a proxy.|Yêu cầu client phải xác thực sử dụng một proxy. Khi máy chủ trả về phản hồi này, nó cũng chỉ ra proxy mà người yêu cầu phải sử dụng.
408|Request Timeout|This response is sent on an idle connection by some servers, even without any previous request by the client.|Request tốn thời gian dài hơn thời gian Server phản hồi.
409|Conflict|This response is sent when a request conflicts with the current state of the server.|Các máy chủ gặp phải một cuộc xung đột thực hiện yêu cầu. Các máy chủ phải bao gồm thông tin về các cuộc xung đột trong các phản ứng. Máy chủ có thể trả về mã này để đáp ứng với yêu cầu PUT xung đột với yêu cầu trước đó, cùng với một danh sách các sự khác biệt giữa các yêu cầu.
410|Gone|This response is sent when the requested content has been permanently deleted from server, with no forwarding address.|Các máy chủ trả về phản hồi này khi các nguồn tài nguyên yêu cầu đã bị loại bỏ vĩnh viễn. Nó tương tự như một 404 (Không tìm thấy) mã, nhưng đôi khi được sử dụng ở vị trí của một 404 cho nguồn lực được sử dụng để tồn tại nhưng không còn làm. Nếu tài nguyên đã di chuyển vĩnh viễn, bạn nên sử dụng một 301 để xác định vị trí mới của tài nguyên.
411|Length Required|Server rejected the request because the Content-Length header field is not defined and the server requires it.|Content-Length không được xác định rõ. Server sẽ không chấp nhận yêu cầu mà không có nó.
412|Precondition Failed|Access to the target resource has been denied.|Các máy chủ không đáp ứng một trong các điều kiện tiên quyết mà người yêu cầu đưa vào yêu cầu.
413|Payload Too Large|Request entity is larger than limits defined by server.|Máy chủ không thể xử lý yêu cầu bởi vì nó là quá lớn đối với các máy chủ để xử lý.
414|Request-URI Too Long|The URI requested by the client is longer than the server is willing to interpret.|URI yêu cầu (thường là một URL) là quá dài đối với máy chủ để xử lý.
415|Unsupported Media Type|The media format is not supported by the server.|Định dạng phương tiện không được máy chủ hỗ trợ.
416|Requested Range Not Satisfiable|The range specified by the Range header field in the request cannot be fulfilled.|Máy chủ trả về mã trạng thái này nếu yêu cầu cho một phạm vi không có sẵn cho trang.
417|Expectation Failed|the expectation indicated by the Expect request header field cannot be met by the server.|Máy chủ không thể đáp ứng yêu cầu của các trường yêu cầu, tiêu đề mong đợi.
418|I'm a teapot|The server refuses the attempt to brew coffee with a teapot.
421|Misdirected Request|The request was directed at a server that is not able to produce a response.|Yêu cầu được hướng đến một máy chủ không thể tạo ra phản hồi.
422|Unprocessable Entity|The request was well-formed but was unable to be followed due to semantic errors.|Yêu cầu đã được hình thành tốt nhưng không thể được thực hiện do lỗi ngữ nghĩa.
423|Locked|The resource that is being accessed is locked.|Tài nguyên đang được truy cập bị khóa.
424|Failed Dependency|The request failed due to failure of a previous request.|Yêu cầu không thành công do yêu cầu trước đó không thành công.
426|Upgrade Required|The server refuses to perform the request using the current protocol but might be willing to do so after the client upgrades to a different protocol.|Máy chủ từ chối thực hiện yêu cầu bằng giao thức hiện tại nhưng có thể sẵn sàng làm như vậy sau khi client nâng cấp lên một giao thức khác.
428|Precondition Required|his response is intended to prevent the 'lost update' problem, where a client GETs a resource's state, modifies it and PUTs it back to the server, when meanwhile a third party has modified the state on the server, leading to a conflict.|phản hồi của anh ta nhằm ngăn chặn vấn đề 'cập nhật bị mất', trong đó client GETs trạng thái của tài nguyên, sửa đổi nó và PUT nó trở lại máy chủ, khi đó bên thứ ba đã sửa đổi trạng thái trên máy chủ, dẫn đến xung đột.
429|Too Many Requests|The user has sent too many requests in a given amount of time.|Người dùng đã gửi quá nhiều yêu cầu trong một khoảng thời gian nhất định.
431|Request Header Fields Too Large|The server is can't process the request because its header fields are too large.|Máy chủ không thể xử lý yêu cầu vì các trường tiêu đề của nó quá lớn.
444|Connection Closed Without Response|The connection opened, but no data was written.|Kết nối đã mở, nhưng không có dữ liệu nào được ghi.
451|Unavailable For Legal Reasons|The user agent requested a resource that cannot legally be provided (such as a web page censored by a government)|Tác nhân người dùng đã yêu cầu một tài nguyên không thể được cung cấp một cách hợp pháp (chẳng hạn như một trang web bị chính phủ kiểm duyệt)
499|Client Closed Request|The client closed the connection, despite the server was processing the request already.|Client đã đóng kết nối, mặc dù máy chủ đã xử lý yêu cầu.
500|Internal Server Error|The server has encountered a situation it does not know how to handle.|Các máy chủ gặp lỗi và không thể thực hiện yêu cầu.
501|Not Implemented|The request method is not supported by the server and cannot be handled.|Các máy chủ không có các chức năng để thực hiện yêu cầu. Ví dụ, máy chủ có thể trả về mã này khi nó không nhận ra phương thức yêu cầu.
502|Bad Gateway|This error response means that the server, while working as a gateway to get a response needed to handle the request, got an invalid response.|Các máy chủ đã hoạt động như một gateway hoặc proxy và nhận được một phản ứng không hợp lệ từ máy chủ ngược.
503|Service Unavailable|The server is not ready to handle the request.|Máy chủ hiện không có sẵn (vì nó bị quá tải hoặc xuống để bảo trì). Nói chung, đây là một trạng thái tạm thời.
504|Gateway Timeout|This error response is given when the server is acting as a gateway and cannot get a response in time.|Các máy chủ đã hoạt động như một gateway hoặc proxy và đã không nhận được yêu cầu kịp thời từ máy chủ ngược.
505|HTTP Version Not Supported|The HTTP version used in the request is not supported by the server.|Các máy chủ không hỗ trợ phiên bản giao thức HTTP được sử dụng trong yêu cầu.
506|Variant Also Negotiates|the chosen variant resource is configured to engage in transparent content negotiation itself, and is therefore not a proper end point in the negotiation process.|Tài nguyên biến thể đã chọn được định cấu hình để tham gia vào chính cuộc đàm phán nội dung minh bạch và do đó không phải là điểm kết thúc thích hợp trong quá trình đàm phán.
507|Insufficient Storage|The method could not be performed on the resource because the server is unable to store the representation needed to successfully complete the request.|Phương pháp không thể được thực hiện trên tài nguyên vì máy chủ không thể lưu trữ biểu diễn cần thiết để hoàn thành thành công yêu cầu.
508|Loop Detected|The server detected an infinite loop while processing the request.|Máy chủ đã phát hiện một vòng lặp vô hạn trong khi xử lý yêu cầu.
510|Not Extended|Further extensions to the request are required for the server to fulfill it.|Các tiện ích mở rộng hơn nữa cho yêu cầu được yêu cầu để máy chủ thực hiện nó.
511|Network Authentication Required|Indicates that the client needs to authenticate to gain network access.|Chỉ ra rằng client cần xác thực để có quyền truy cập mạng.
599|Network Connect Timeout Error|The connection timed out due to a overloaded server, a hardware error or a infrastructure error.|Kết nối hết thời gian chờ do máy chủ quá tải, lỗi phần cứng hoặc lỗi cơ sở hạ tầng.

---
# HTTPS

>Hypertext Transfer Protocol Secure

- là một phần mở rộng của Hypertext Transfer Protocol (HTTP)
- Nó được sử dụng để giao tiếp an toàn qua mạng máy tính và được sử dụng rộng rãi trên Internet.
- Trong HTTPS, giao thức truyền thông được mã hóa bằng Transport Layer Security (TLS) hay trước đây là Secure Sockets Layer (SSL). Do đó, giao thức này còn được gọi là HTTP qua TLS, hoặc HTTP qua SSL.

Các động cơ chính để dùng HTTPS là xác thực trang web được truy cập và bảo vệ quyền riêng tư và tính toàn vẹn của dữ liệu được trao đổi trong khi truyền.

Nó bảo vệ chống lại các cuộc tấn công của kẻ trung gian và mã hóa hai chiều của giao tiếp giữa máy khách và máy chủ bảo vệ thông tin liên lạc khỏi bị nghe trộm và giả mạo. 

Khía cạnh xác thực của HTTPS yêu cầu một bên thứ ba đáng tin cậy ký các chứng chỉ số phía máy chủ.

Trước đây, đây là một hoạt động tốn kém, có nghĩa là các kết nối HTTPS đã được xác thực hoàn toàn thường chỉ được tìm thấy trên các dịch vụ giao dịch thanh toán bảo đảm và các hệ thống thông tin công ty được bảo mật khác trên World Wide Web.

Vào năm 2016, một chiến dịch của Electronic Frontier Foundation với sự hỗ trợ của các nhà phát triển trình duyệt web đã khiến giao thức này trở nên phổ biến hơn.

HTTPS hiện được người dùng web sử dụng thường xuyên hơn so với HTTP không an toàn ban đầu, chủ yếu để bảo vệ tính xác thực của trang trên tất cả các loại trang web; tài khoản an toàn; và giữ bí mật thông tin liên lạc, danh tính và duyệt web của người dùng.

---
# DNS


---
# SSH


---
# 