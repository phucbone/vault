![[hashicorp-terraform-presend.jpg]](https://gitlab.com/phucbone/vault/-/raw/main/imgs/imgs-hashicorp-stack/imgs-hashicorp-terraform/hashicorp-terraform-presend.jpg)

# Giới thiệu

`Terraform` là công cụ đầu tiên ta cần học trong bộ công cụ `Hashicorp Stack`. 

`Terraform` sẽ giúp ta tự động hóa công việc tạo hạ tầng trên môi trường `Cloud`.

![[hashicorp-terraform-workflow.jpg]](https://gitlab.com/phucbone/vault/-/raw/main/imgs/imgs-hashicorp-stack/imgs-hashicorp-terraform/hashicorp-terraform-workflow.jpg)