Linux Commands

No|Tool
---|---
1|[[awk]](https://gitlab.com/phucbone/vault/-/blob/main/linux-commands/awk.md)
2|[[wc]](https://gitlab.com/phucbone/vault/-/blob/main/linux-commands/wc.md)
3|[[egrep]](https://gitlab.com/phucbone/vault/-/blob/main/linux-commands/egrep.md)
4|[[sed]](https://gitlab.com/phucbone/vault/-/blob/main/linux-commands/sed.md)

