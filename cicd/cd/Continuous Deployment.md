Continuous Deployment Tools

No|Tool
---|---
1|[[argocd]](https://gitlab.com/phucbone/vault/-/blob/main/cicd/cd/argocd.md)
2|[[armory-spinnaker]](https://gitlab.com/phucbone/vault/-/blob/main/cicd/cd/armory-spinnaker.md)
3|[[chef]](https://gitlab.com/phucbone/vault/-/blob/main/cicd/cd/chef.md)
4|[[digital.ai-deploy]](https://gitlab.com/phucbone/vault/-/blob/main/cicd/cd/digital.ai-deploy.md)
5|[[gocd]](https://gitlab.com/phucbone/vault/-/blob/main/cicd/cd/gocd.md)
6|[[ibm-cloud-devops]](https://gitlab.com/phucbone/vault/-/blob/main/cicd/cd/ibm-cloud-devops.md)
7|[[micro-focus]](https://gitlab.com/phucbone/vault/-/blob/main/cicd/cd/micro-focus.md)
8|[[octopus-deploy]](https://gitlab.com/phucbone/vault/-/blob/main/cicd/cd/octopus-deploy.md)
9|[[spinnaker]](https://gitlab.com/phucbone/vault/-/blob/main/cicd/cd/spinnaker.md)

